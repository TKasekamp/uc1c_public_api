export default () => ({
  cas: {
    url: 'https://devpms.arib.pria.ee/cas/v1/tickets',
    name: 'https://devpms.arib.pria.ee/pms-menetlus/',
    userName: process.env.CAS_USERNAME,
    password: process.env.CAS_PASSWORD,
  },
});
