import { Test, TestingModule } from '@nestjs/testing';
import { PoldService } from './pold.service';
import { PriaClient } from './pria.client';
import { PriaModule } from './pria.module';
import { AxiosResponse } from 'axios';
import * as pollud from '../../sample_data/pollud.json';
import { ConfigModule } from '@nestjs/config';
import configuration from '../configuration';
import { LoggerModule } from 'nestjs-pino';

describe('PoldService', () => {
  let service: PoldService;
  let priaClient: PriaClient;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [LoggerModule.forRoot(), PriaModule, ConfigModule.forRoot({
        load: [configuration],
        isGlobal: true,
      })],
      providers: [PoldService],
    }).compile();

    service = module.get<PoldService>(PoldService);
    priaClient = module.get<PriaClient>(PriaClient);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return client', async () => {
    const result: AxiosResponse = {
      data: pollud,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest.spyOn(priaClient, 'get').mockResolvedValue(result);
    const response = await service.findByClient(605104);
    expect(response.length).toEqual(15);
    expect(priaClient.get).toHaveBeenCalledWith(expect.any(String), { params: { klientId: 605104, pageNumber: 1, pageSize: 5 } });
  });
});
