import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios, { AxiosRequestConfig } from 'axios';
import { PinoLogger } from 'nestjs-pino';

@Injectable()
export class PriaClient {
  stTicket: string;

  constructor(private readonly logger: PinoLogger, private configService: ConfigService) {
    this.logger.setContext('PriaClient');
  }

  async get(url, config?) {
    try {
      return await axios.get(url, await this.getConfig(config));
    } catch (error) {
      if (error.response.status === 401) {
        this.logger.info('Ticket expired, clearing from store before next request');
        this.stTicket = null;
        return await axios.get(url, await this.getConfig(config));
      }
      return Promise.reject(error);
    }
  }

  private async getConfig(config?: AxiosRequestConfig): Promise<AxiosRequestConfig> {
    const headers = {
      Authorization: 'Pria ticket=' + await this.getStTicket(),
    };
    if (!config) {
      config = {};
    }
    if (config.headers) {
      config.headers = headers;
    } else {
      config.headers = { ...config.headers, ...headers };
    }
    return config;
  }

  private async getStTicket(): Promise<string> {
    if (this.stTicket) {
      this.logger.info('ST ticket already in store');
      return Promise.resolve(this.stTicket);
    }

    const conf = this.configService.get<object>('cas');
    // @ts-ignore
    const tgtRequestBody = 'username=' + encodeURIComponent(conf.userName) + '&password=' + encodeURIComponent(conf.password);

    const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
    // @ts-ignore
    const tgtRequest = axios.post(conf.url, tgtRequestBody, { headers });

    const tgtTicket = await tgtRequest.then(({ data }) => data);
    this.logger.info('Retrieved TGT Ticket');

    // @ts-ignore
    const stRequestBody = 'service=' + encodeURIComponent(conf.name);

    // @ts-ignore
    const s = axios.post(conf.url + '/' + tgtTicket, stRequestBody, { headers });
    this.stTicket = await s.then(({ data }) => data);
    this.logger.info('Retrieved ST ticket');
    return Promise.resolve(this.stTicket);
  }
}
