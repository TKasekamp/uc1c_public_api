import { Test, TestingModule } from '@nestjs/testing';
import { KlientService } from './klient.service';
import { LoggerModule } from 'nestjs-pino';
import { PriaModule } from './pria.module';
import { ConfigModule } from '@nestjs/config';
import configuration from '../configuration';
import * as clients from '../../sample_data/clients.json';
import { AxiosResponse } from 'axios';
import { PriaClient } from './pria.client';

describe('KlientService', () => {
  let service: KlientService;
  let priaClient: PriaClient;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [LoggerModule.forRoot(), PriaModule, ConfigModule.forRoot({
        load: [configuration],
        isGlobal: true,
      })],
      providers: [KlientService],
    }).compile();

    service = module.get<KlientService>(KlientService);
    priaClient = module.get<PriaClient>(PriaClient);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return client', async () => {
    const result: AxiosResponse = {
      data: { content: clients },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest.spyOn(priaClient, 'get').mockResolvedValue(result);
    const client = await service.findClient('123');
    expect(client.id).toEqual(605104);
    expect(priaClient.get).toHaveBeenCalledWith(expect.any(String), { params: { isikukood: '123', pageNumber: 1, pageSize: 5 } });
  });
});
