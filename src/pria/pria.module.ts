import { HttpModule, Module } from '@nestjs/common';
import { KlientService } from './klient.service';
import { PoldService } from './pold.service';
import { PriaClient } from './pria.client';

@Module({
  imports: [HttpModule],
  providers: [KlientService, PoldService, PriaClient],
  exports: [KlientService, PoldService, PriaClient],
})
export class PriaModule {}
