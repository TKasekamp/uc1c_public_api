export interface Pold {
  WKT: string;
  AASTA: number;
  POLLU_ID: number;
  KLIENT_KOOD: number;
  POLLU_PIND: any;
  MUUTMISE_AEG: string;
  POLLUKULTUUR_KOOD: string;
  NIITMISE_TUVASTAMINE_STAATUS: string;
  NIITMISE_VARASEIM_AEG: string;
  NIITMISE_HILISEIM_AEG: string;
}
