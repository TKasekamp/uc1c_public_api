export interface TapsustPollukultuurid {
  id: any;
  pollukultuurKood: number;
  tapsustatudKultuurNimetus: string;
  tapsustatudKultuurKood: number;
  aasta: number;
  maakasutusPollukultuur: string;
}

export interface Pollukultuur {
  pollukultuurKood: number;
  maakasutusPollukultuur: string;
  pollukultuur: string;
  maakasutusTyyp: string;
  maakasutusTyypNimetus: string;
  tapsustPollukultuurid: TapsustPollukultuurid[];
  isTapsustatav: boolean;
  heintaim: number;
  onPraktika: number;
  praktikaKoefitsient: number;
  onLammastikkuSiduv: number;
  onKesa: number;
  onHkHv: number;
  onHeintaimeSegu: number;
  onTapsustKultuurVabatekst: number;
  aasta: number;
}

export interface PollukultuurOtstarve {
  id: number;
  kood: string;
  nimetus: string;
  aasta: number;
  kirjeldus: string;
}

export interface PollukultuuriTapsustused {
  poldId: number;
  tapsustPollukultuurId: number;
  pollukultuurKood: number;
  tapsustatudKultuurNimetus: string;
  tapsustatudKultuurKood: number;
  aasta: number;
  sisestatudKultuur?: any;
  osakaal: number;
  lisamisAeg: any;
  valitud: boolean;
  maakasutusPollukultuur: string;
}

export interface PoldKasutusOigus {
  id: number;
  kood: string;
  nimetus: string;
  aasta: number;
  kirjeldus: string;
}

export interface MaakasutusTyyp {
  id: any;
  kood: string;
  nimetus: string;
  aasta: number;
}

export interface MaheMaakasutus {
  id: number;
  mahemkLyhend: string;
  mahemkKirjeldus: string;
  aasta: number;
}

export interface Geometry {
  type: string;
  coordinates: number[][][];
}

export interface PollukultuurOtstarve2 {
  id: number;
  kood: string;
  nimetus: string;
  aasta: number;
  kirjeldus: string;
}

export interface MaheMaakasutus2 {
  id: number;
  mahemkLyhend: string;
  mahemkKirjeldus: string;
  aasta: number;
}

export interface MaakasutusTyyp2 {
  id: any;
  kood: string;
  nimetus: string;
  aasta: number;
}

export interface PollukultuuriTapsustused2 {
  poldId: number;
  tapsustPollukultuurId: number;
  pollukultuurKood: number;
  tapsustatudKultuurNimetus: string;
  tapsustatudKultuurKood: number;
  aasta: number;
  sisestatudKultuur?: any;
  osakaal: number;
  lisamisAeg: any;
  valitud: boolean;
  maakasutusPollukultuur: string;
}

export interface PoldKasutusoigus {
  id: number;
  kood: string;
  nimetus: string;
  aasta: number;
  kirjeldus: string;
}

export interface TapsustPollukultuurid2 {
  id: any;
  pollukultuurKood: number;
  tapsustatudKultuurNimetus: string;
  tapsustatudKultuurKood: number;
  aasta: number;
  maakasutusPollukultuur: string;
}

export interface Pollukultuur2 {
  pollukultuurKood: number;
  maakasutusPollukultuur: string;
  pollukultuur: string;
  maakasutusTyyp: string;
  maakasutusTyypNimetus: string;
  tapsustPollukultuurid: TapsustPollukultuurid2[];
  isTapsustatav: boolean;
  heintaim: number;
  onPraktika: number;
  praktikaKoefitsient: number;
  onLammastikkuSiduv: number;
  onKesa: number;
  onHkHv: number;
  onHeintaimeSegu: number;
  onTapsustKultuurVabatekst: number;
  aasta: number;
}

export interface Properties {
  pollukultuurOtstarve: PollukultuurOtstarve2;
  maheMaakasutus: MaheMaakasutus2;
  maakasutusTyyp: MaakasutusTyyp2;
  poldPindala: number;
  taotlusAlusala: string;
  lisaja: number;
  maakasutusMuutmineSelgitus?: any;
  okoalaKattumineSelgitus: string;
  ylePiiriSelgitus?: any;
  klientId: number;
  staatus: number;
  onYlePiiriJoonistatud: boolean;
  onPiiristValjaJoonistatud: boolean;
  lisamisAeg: any;
  muutja: number;
  id: number;
  pollukultuuriTapsustused: PollukultuuriTapsustused2[];
  onKultuuridRidades: boolean;
  poldKasutusoigus: PoldKasutusoigus;
  taotluselUuendatudKp?: any;
  isTapsustatavPollukultuur: boolean;
  maaalaNr: string;
  massiivXyId: string;
  onTprKattub: boolean;
  onKindlaksTehtud: boolean;
  okoalaKattuminePindala: number;
  katastritunnus?: any;
  poldNr: string;
  muutmisAeg: any;
  toetusKategooria?: any;
  pollukultuur: Pollukultuur2;
  kirjeldus: string;
}

export interface Geom {
  type: string;
  geometry: Geometry;
  properties: Properties;
}

export interface Pold {
  id: number;
  klientId: number;
  poldNr: string;
  massiivXyId: string;
  maaalaNr: string;
  katastritunnus?: any;
  poldPindala: number;
  pollukultuur: Pollukultuur;
  pollukultuurOtstarve: PollukultuurOtstarve;
  pollukultuuriTapsustused: PollukultuuriTapsustused[];
  isTapsustatavPollukultuur: boolean;
  poldKasutusOigus: PoldKasutusOigus;
  taotlusAlusala: string;
  taotluselUuendatudKp?: any;
  toetusKategooria?: any;
  onYlePiiriJoonistatud: boolean;
  ylePiiriSelgitus?: any;
  onKindlaksTehtud: boolean;
  onPiiristValjaJoonistatud: boolean;
  maakasutusTyyp: MaakasutusTyyp;
  maakasutusMuutmineSelgitus?: any;
  onKultuuridRidades: boolean;
  onTprKattub: boolean;
  okoalaKattuminePindala: number;
  okoalaKattumineSelgitus: string;
  maheMaakasutus: MaheMaakasutus;
  kirjeldus: string;
  staatus: number;
  satPoldStaatusega?: any;
  lisaja: number;
  lisamisAeg: any;
  muutja: number;
  muutmisAeg: any;
  geom: Geom;
}
