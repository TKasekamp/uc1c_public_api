import { Injectable, NotFoundException } from '@nestjs/common';
import { Klient } from './interfaces/klient.interface';
import { PriaClient } from './pria.client';
import { PinoLogger } from 'nestjs-pino';

@Injectable()
export class KlientService {
  constructor(private readonly logger: PinoLogger, private priaClient: PriaClient) {
    this.logger.setContext('KlientService');
  }

  async findClient(kood: string): Promise<Klient> {
    this.logger.info(`Searching for client by ${kood}`);
    const url = 'https://devpms.arib.pria.ee/klient/ext/klient/otsing';
    const response = await this.priaClient.get(url, { params: { isikukood: kood, pageNumber: 1, pageSize: 5 } });
    let clients = response.data.content;
    if (clients.length === 0) {
      this.logger.info('Unable to find by isikukood, searching by registrikood');
      const response2 = await this.priaClient.get(url, { params: { registrikood: kood, pageNumber: 1, pageSize: 5 } });
      clients = response2.data.content;
    }
    this.logger.info(`Retrieved ${clients.length} clients`);
    if (!clients[0]) {
      throw new NotFoundException('Client not found');
    }
    return clients[0];
  }
}
