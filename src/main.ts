import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { Logger } from 'nestjs-pino';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: false });
  app.useGlobalPipes(new ValidationPipe({
    forbidNonWhitelisted: true,
    whitelist: true,
  }));
  app.setGlobalPrefix('api/v1/ecrop');

  const options = new DocumentBuilder()
    .setTitle('NIVA example')
    .setDescription('The NIVA API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('', app, document);

  app.useLogger(app.get(Logger));

  await app.listen(process.env.PORT || 3000);
  // Starts listening for shutdown hooks
  app.enableShutdownHooks();
}

bootstrap();
