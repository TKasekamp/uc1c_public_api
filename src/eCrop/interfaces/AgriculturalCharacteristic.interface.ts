import { ApiProperty } from '@nestjs/swagger';
import { CodeType } from './CodeType.interface';

export class AgriculturalCharacteristic {
  @ApiProperty({required: false})
  TypeCode?: CodeType;
  @ApiProperty({required: false})
  ValueCode?: CodeType;
  @ApiProperty({required: false})
  ValueIndicator?: boolean;
}
