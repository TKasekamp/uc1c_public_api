import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CodeType {
  @ApiProperty({required: true, example: 'JP'})
  @IsString()
  Code: string;
  @ApiProperty({required: false})
  @IsString()
  @IsOptional()
  Name?: string;
}
