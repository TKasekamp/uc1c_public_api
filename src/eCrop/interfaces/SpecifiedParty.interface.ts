import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsInt, IsOptional, IsString } from 'class-validator';

export class SpecifiedPartyInterface {
  @ApiProperty({ example: 1, examples: [1, 2], description: 'Recipient ID. PRIA 1, eAgronom 2' })
  @IsInt()
  @IsIn([1, 2])
  ID: number;
  @ApiProperty({ description: 'Recipient Name', required: false })
  @IsString()
  @IsOptional()
  Name?: string;
}
