import { ApiProperty } from '@nestjs/swagger';
import { CropProductionAgriculturalProcess } from './CropProductionAgriculturalProcess.interface';
import { CodeType } from './CodeType.interface';
import { ClassificationCode } from './ClassificationCode.interface';

export class FieldCrop {
  @ApiProperty()
  LandUseCode?: CodeType;
  @ApiProperty({ description: 'The code specifying the sowing period for this field crop, such as spring or winter.' })
  SowingPeriodCode?: CodeType;
  @ApiProperty({ type: [CodeType], description: 'A code specifying a purpose for this field crop.'})
  PurposeCode?: CodeType[];
  @ApiProperty({ type: [CodeType], description: 'A code specifying a reason for planting this field crop.' })
  PlantingReasonCode?: CodeType[];
  @ApiProperty({ type: [ClassificationCode], description: 'A code specifying a classification for this field crop.' })
  ClassificationCode?: ClassificationCode[];
  @ApiProperty({ type: [ClassificationCode] })
  SubClassificationCode?: ClassificationCode[];
  @ApiProperty({ description: 'The code specifying the type of cultivation for this field crop.' })
  CultivationTypeCode?: string;
  @ApiProperty({ description: 'The indication of whether or not a field crop is to be used as propagation material.' })
  PropagationMaterialIndicator?: string;
  @ApiProperty({ type: [String], description: 'A code specifying a type of cultivation coverage, such as glass, for this field crop.' })
  CultivationCoverageCode?: string[];
  @ApiProperty({ description: 'The code specifying the production period for this field crop.' })
  ProductionPeriodCode?: string;
  @ApiProperty({ description: 'The code specifying the production environment for this field crop.' })
  ProductionEnvironmentCode?: string;
}
