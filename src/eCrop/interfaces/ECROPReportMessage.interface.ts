import { ApiProperty } from '@nestjs/swagger';
import { CropReportDocument } from './CropReportDocument.interface';
import { AgriculturalProducerParty } from './AgriculturalProducerParty.interface';

export class ECROPReportMessage {
  @ApiProperty()
  CropReportDocument: CropReportDocument;
  @ApiProperty()
  AgriculturalProducerParty: AgriculturalProducerParty;
}
