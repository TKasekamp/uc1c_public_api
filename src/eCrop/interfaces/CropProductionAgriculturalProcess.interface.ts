import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CodeType } from './CodeType.interface';

export class CropProductionAgriculturalProcess {
  @ValidateNested()
  @Type(() => CodeType)
  @ApiProperty({ example: 1, description: 'Moving is 1', required: true })
  TypeCode: CodeType;
  @ApiProperty({ example: '2020-06-27T13:10:20Z', description: 'Earliest start time', required: false })
  @IsDateString()
  EarliestStartDateTime?: string;
  @ApiProperty({ example: '2020-06-27T13:10:20Z', description: 'Actual start time', required: true })
  @IsDateString()
  ActualStartDateTime: string;
  @ApiProperty({ example: '2020-06-02T13:10:20Z', description: 'Latest end time', required: true })
  @IsDateString()
  LatestEndDateTime: string;
  @ApiProperty({ example: '2020-06-02T13:10:20Z', description: 'Latest end time', required: false })
  @IsDateString()
  ActualEndDateTime?: string;
  @ApiProperty({ example: 'Job well done', description: 'Additional information about performed task, described in field book.', required: false })
  Description?: string;
}
