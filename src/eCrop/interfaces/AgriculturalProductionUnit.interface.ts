import { ApiProperty } from '@nestjs/swagger';
import { GrownCropPlot } from './GrownCropPlot.interface';

export class AgriculturalProductionUnit {
  @ApiProperty()
  GrownCropPlot: GrownCropPlot;
}
