import { ApiProperty } from '@nestjs/swagger';
import { FieldCrop } from './FieldCrop.interface';
import { ReferencedLocation } from './location/ReferencedLocation.interface';
import { AgriculturalCharacteristic } from './AgriculturalCharacteristic.interface';
import { CodeType } from './CodeType.interface';
import { CropProductionAgriculturalProcess } from './CropProductionAgriculturalProcess.interface';

export class GrownCropPlot {
  @ApiProperty({ example: 123, description: 'Field ID in PRIA system' })
  ID: number;

  @ApiProperty()
  ObjectTypeCode: CodeType;
  @ApiProperty()
  ParcelNr: string;
  @ApiProperty()
  SubParcelNr?: number;
  @ApiProperty()
  CountryCode?: string;
  @ApiProperty()
  ReferenceCadastralUnitNr?: string;
  @ApiProperty()
  ReferenceParcelNr?: string;

  @ApiProperty({ example: 0.41 })
  AreaMeasure?: number;
  @ApiProperty({ type: [AgriculturalCharacteristic] })
  SpecifiedAgriculturalCharacteristic: AgriculturalCharacteristic[];

  @ApiProperty()
  RegulatoryOrganicIndicator?: number;
  @ApiProperty({ type: [ReferencedLocation], description: 'Locations' })
  SpecifiedReferencedLocation: ReferencedLocation[];
  @ApiProperty({ type: [FieldCrop], description: 'Field crops grown on this crop plot.' })
  GrownFieldCrop: FieldCrop[];

  @ApiProperty({ type: [CropProductionAgriculturalProcess], description: 'Processes on this field' })
  ApplicableCropProductionAgriculturalProcess?: CropProductionAgriculturalProcess[];
}
