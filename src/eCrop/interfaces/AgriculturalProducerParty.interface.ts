import { ApiProperty } from '@nestjs/swagger';
import { AgriculturalProductionUnit } from './AgriculturalProductionUnit.interface';

export class AgriculturalProducerParty {
  @ApiProperty({ example: 123, description: 'Client ID in PRIA' })
  ID: number;

  @ApiProperty({type: [AgriculturalProductionUnit]})
  ManagedAgriculturalProductionUnit: AgriculturalProductionUnit[];
}
