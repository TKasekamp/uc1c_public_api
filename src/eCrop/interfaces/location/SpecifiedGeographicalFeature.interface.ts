import { ApiProperty } from '@nestjs/swagger';

export class SpecifiedGeographicalFeature {
  @ApiProperty({ description: 'GeoJSON' })
  IncludedSpecifiedPolygon?: object;

  @ApiProperty({ default: 'EPSG:3301' })
  CoordinateReferenceSystemID?: string;
}
