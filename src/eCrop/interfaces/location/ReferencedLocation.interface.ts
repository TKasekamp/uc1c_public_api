import { ApiProperty } from '@nestjs/swagger';
import { SpecifiedGeographicalFeature } from './SpecifiedGeographicalFeature.interface';
import { ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class ReferencedLocation {
  @ApiProperty()
  @ValidateNested()
  @Type(() => SpecifiedGeographicalFeature)
  PhysicalSpecifiedGeographicalFeature?: SpecifiedGeographicalFeature;
}
