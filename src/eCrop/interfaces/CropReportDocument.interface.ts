import { ApiProperty } from '@nestjs/swagger';
import { SpecifiedPartyInterface } from './SpecifiedParty.interface';
import { IsDateString } from 'class-validator';

export class CropReportDocument {
  @ApiProperty({ example: 12345, description: 'Document ID' })
  ID?: number;
  @ApiProperty()
  @IsDateString()
  IssueDateTime?: string;
  @ApiProperty()
  @IsDateString()
  CopyIndicator?: string;
  @ApiProperty()
  ControlRequirementIndicator?: string;
  @ApiProperty()
  SenderSpecifiedParty: SpecifiedPartyInterface;
  @ApiProperty()
  RecipientSpecifiedParty: SpecifiedPartyInterface;
}
