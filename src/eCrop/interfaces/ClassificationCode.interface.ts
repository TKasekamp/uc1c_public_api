import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class ClassificationCode {
  @ApiProperty({required: true, example: 3})
  @IsNumber()
  Code: number;
  @ApiProperty({required: false})
  @IsString()
  @IsOptional()
  Name?: string;
}
