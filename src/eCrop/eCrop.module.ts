import { Module } from '@nestjs/common';
import { CropPlotController } from './cropPlot.controller';
import { PriaModule } from '../pria/pria.module';
import { CropPlotService } from './cropPlot.service';

@Module({
  providers: [CropPlotService],
  imports: [PriaModule],
  controllers: [CropPlotController],
})
export class ECropModule {}
