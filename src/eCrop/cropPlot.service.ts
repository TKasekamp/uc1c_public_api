import { Injectable } from '@nestjs/common';
import { Pold } from 'src/pria/interfaces/pold.interface';
import { GrownCropPlot } from './interfaces/GrownCropPlot.interface';
import {
  CropProductionAgriculturalProcess,
} from './interfaces/CropProductionAgriculturalProcess.interface';
import { ECROPReportMessage } from './interfaces/ECROPReportMessage.interface';
import { AgriculturalProducerParty } from './interfaces/AgriculturalProducerParty.interface';
import { CropReportDocument } from './interfaces/CropReportDocument.interface';
import { FieldCrop } from './interfaces/FieldCrop.interface';
import { KlientService } from '../pria/klient.service';
import { PoldService } from '../pria/pold.service';
import { Klient } from '../pria/interfaces/klient.interface';
import { AgriculturalCharacteristic } from './interfaces/AgriculturalCharacteristic.interface';
import { CodeType } from './interfaces/CodeType.interface';
import { ClassificationCode } from './interfaces/ClassificationCode.interface';

@Injectable()
export class CropPlotService {

  constructor(private readonly klientService: KlientService,
              private readonly poldService: PoldService) {
  }

  async findByClientPersonalCode(personalCode: string): Promise<ECROPReportMessage[]> {
    const client = await this.klientService.findClient(personalCode);
    const pollud = await this.poldService.findByClient(client.id);

    return [this.poldToMessage(pollud, client)];
  }

  poldToMessage(pollud: Pold [], client: Klient): ECROPReportMessage {
    return {
      AgriculturalProducerParty: this.toParty(pollud, client),
      CropReportDocument: this.toDocument(),
    };
  }

  toDocument(): CropReportDocument {
    return {
      RecipientSpecifiedParty: { ID: 2 }, SenderSpecifiedParty: { ID: 1 },
    };
  }

  toCropPlot(pold: Pold) : GrownCropPlot {
    const fieldCrop: FieldCrop = {
      ClassificationCode: [{ Code: pold.pollukultuur.pollukultuurKood, Name: pold.pollukultuur.pollukultuur }],
      LandUseCode: this.landUseCode(pold),
      PurposeCode: this.purposeCode(pold),
      SubClassificationCode: this.subClassificationCodes(pold),
    };
    return {
      SpecifiedAgriculturalCharacteristic: this.characteristics(pold),
      ID: pold.id,
      ObjectTypeCode: this.objectType(pold),
      ParcelNr: pold.poldNr,
      SpecifiedReferencedLocation: [{
        PhysicalSpecifiedGeographicalFeature: {
          CoordinateReferenceSystemID: 'EPSG:3301',
          IncludedSpecifiedPolygon: pold.geom.geometry,
        },
      }],
      GrownFieldCrop: [fieldCrop],
      AreaMeasure: pold.poldPindala,
      ReferenceCadastralUnitNr: pold.katastritunnus,
      ReferenceParcelNr: pold.massiivXyId,
      ApplicableCropProductionAgriculturalProcess: this.toProcess(pold),
    };
  }

  toParty(pollud: Pold [], client: Klient): AgriculturalProducerParty {
    return {
      ID: this.producerPartyCode(client),
      ManagedAgriculturalProductionUnit: pollud.map(pold => ({GrownCropPlot: this.toCropPlot(pold)})),
    };
  }

  toProcess(pold: Pold): CropProductionAgriculturalProcess[] {
    return [];
  }

  characteristics(pold: Pold): AgriculturalCharacteristic[] {
    let characteristics = [];

    if (pold.maheMaakasutus?.mahemkLyhend) {
      characteristics = [...characteristics,
        {
          TypeCode: { Code: 'MM', Name: 'mahe maakasutus' },
          ValueCode: { Code: pold.maheMaakasutus.mahemkLyhend, Name: pold.maheMaakasutus.mahemkKirjeldus },
        },
      ];
    }
    if (pold.toetusKategooria?.kood === 'HS') {
      characteristics = [...characteristics,
        { TypeCode: { Code: 'HS', Name: 'heinaseeme' }, ValueIndicator: true },
      ];
    }
    return characteristics;
  }

  producerPartyCode(client: Klient) {
    return client.isikukood || client.registrikood;
  }

  objectType(pold: Pold): CodeType {
    return { Code: 'JP', Name: 'joonistatud põld' };
  }

  landUseCode(pold: Pold): CodeType {
    return { Code: pold.maakasutusTyyp.kood, Name: pold.maakasutusTyyp.nimetus };
  }

  purposeCode(pold: Pold): CodeType[] {
    const Code = pold.pollukultuurOtstarve?.kood;
    if (Code === 'HM') {
      return [{ Code, Name: 'Haljasmassiks' }];
    } else if (Code !== undefined) {
      return [{ Code, Name: pold.pollukultuurOtstarve?.nimetus }];
    }
    return [];
  }

  subClassificationCodes(pold: Pold): ClassificationCode[] {
    return pold.pollukultuuriTapsustused
      .map(t => ({ Code: t.tapsustatudKultuurKood, Name: t.tapsustatudKultuurNimetus }));
  }
}
