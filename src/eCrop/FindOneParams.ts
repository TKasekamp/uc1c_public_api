import { IsNumberString, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FindOneParams {
  @IsNumberString()
  @MinLength(2)
  @ApiProperty({ description: 'Personal code (isikukood) or business code (registrykood)', default: '11952206', minLength: 2 })
  id: string;
}
