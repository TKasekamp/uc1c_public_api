import { Test } from '@nestjs/testing';
import { CropPlotService } from './cropPlot.service';
// @ts-ignore
import * as pollud from '../../sample_data/pollud.json';
import * as pollud2 from '../../sample_data/pollud2.json';
import * as ecropRepsonse2 from '../../sample_data/ecrop-response-2.json';
import * as ecropRepsonse1 from '../../sample_data/ecrop-response-1.json';
import { LoggerModule } from 'nestjs-pino';
import { KlientService } from '../pria/klient.service';
import { PoldService } from '../pria/pold.service';
import * as clients from '../../sample_data/clients.json';
import { PriaModule } from '../pria/pria.module';
import { ConfigModule } from '@nestjs/config';
import configuration from '../configuration';
import { ECropModule } from './eCrop.module';

describe('CropPlotService', () => {
  let cropPlotService: CropPlotService;
  let klientService: KlientService;
  let poldService: PoldService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [LoggerModule.forRoot(), ECropModule, PriaModule, ConfigModule.forRoot({
        load: [configuration],
        isGlobal: true,
      })],
      providers: [CropPlotService, KlientService, PoldService],
    }).compile();

    cropPlotService = module.get<CropPlotService>(CropPlotService);
    klientService = module.get<KlientService>(KlientService);
    poldService = module.get<PoldService>(PoldService);
  });

  describe('findByClientPersonalCode', () => {
    it('returns fields by client personal code', async () => {
      jest.spyOn(klientService, 'findClient').mockResolvedValue(clients[0]);
      jest.spyOn(poldService, 'findByClient').mockResolvedValue(pollud);

      const ecrops = await cropPlotService.findByClientPersonalCode('46104130295');
      expect(klientService.findClient).toHaveBeenCalledWith('46104130295');
      expect(poldService.findByClient).toHaveBeenCalledWith(605104);
      expect(ecrops[0].AgriculturalProducerParty.ManagedAgriculturalProductionUnit.length).toEqual(15);
    });
  });

  describe('poldToCropPlot', () => {
    describe('when no mowing', () => {
      it('should map pold to grown crop plot', () => {
        const message = cropPlotService.poldToMessage(pollud, clients[0]);

        expect(message).toEqual(ecropRepsonse1);
      });
    });

    describe('a real example', () => {
      it('maps to ecrop', async () => {
        const message = cropPlotService.poldToMessage(pollud2, { ...clients[0], isikukood: '38403284254' });

        expect(message).toEqual(ecropRepsonse2[0]);
      });
    });
  });

});
