import { Body, Controller, Get, NotFoundException, Param, Post } from '@nestjs/common';
import { FindOneParams } from './FindOneParams';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiNotFoundResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ECROPReportMessage } from './interfaces/ECROPReportMessage.interface';
import { CropPlotService } from './cropPlot.service';
import { PinoLogger } from 'nestjs-pino';
import { CreateCropsDto } from './dtos/CreateCropsDto';

@ApiTags('Crop plot')
@Controller('crop-plot')
export class CropPlotController {
  constructor(
    private readonly cropPlotService: CropPlotService,
    private readonly logger: PinoLogger,
  ) {
    this.logger.setContext('CropPlotController');
  }

  @Get(':id')
  @ApiOperation({ summary: 'Crop plot by client personal code or business code' })
  @ApiNotFoundResponse({ description: 'Field not found' })
  async findByClientPersonalCode(@Param() params: FindOneParams): Promise<ECROPReportMessage[]> {
    this.logger.info(`Finding field by client personal code ${params.id}`);
    const plot = await this.cropPlotService.findByClientPersonalCode(params.id);
    if (plot) {
      return plot;
    } else {
      throw new NotFoundException();
    }
  }

  @Post()
  @ApiOperation({ summary: 'Create crop plot' })
  @ApiCreatedResponse({description: 'Crop plot created'})
  @ApiBadRequestResponse({description: 'Invalid input'})
  create(@Body() dto: CreateCropsDto) {
    this.logger.info(`Creating crop plot for ${dto.AgriculturalProducerParty.ID}`);
  }

}
