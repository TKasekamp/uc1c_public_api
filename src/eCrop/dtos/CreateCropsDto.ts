/* tslint:disable:max-classes-per-file */
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsArray, IsDateString, IsDefined, IsInt, IsNumberString, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CodeType } from '../interfaces/CodeType.interface';
import { SpecifiedPartyInterface } from '../interfaces/SpecifiedParty.interface';
import { ClassificationCode } from '../interfaces/ClassificationCode.interface';

class WeightMeasure {
  @ApiProperty()
  @IsInt()
  Value: number;
  @ApiProperty()
  @IsString()
  unitCode: string;
}

class SpecifiedAgriculturalInputProduct {
  @ApiProperty()
  @IsInt()
  ID: number;
  @ApiProperty()
  @ValidateNested()
  @Type(() => CodeType)
  @IsDefined()
  TypeCode: CodeType;
  @ApiProperty()
  @IsString()
  Name: string;
}

class SpecifiedProductBatch {
  @ApiProperty()
  @ValidateNested()
  @Type(() => WeightMeasure)
  @IsDefined()
  WeightMeasure: WeightMeasure;
  @ApiProperty({ type: [SpecifiedAgriculturalInputProduct] })
  @ValidateNested({ each: true })
  @Type(() => SpecifiedAgriculturalInputProduct)
  @IsArray()
  @IsDefined()
  SpecifiedAgriculturalInputProduct: SpecifiedAgriculturalInputProduct[];
}

class AppliedSpecifiedAgriculturalApplication {
  @ApiProperty({ type: [SpecifiedProductBatch] })
  @ValidateNested({ each: true })
  @Type(() => SpecifiedProductBatch)
  @IsArray()
  @IsDefined()
  SpecifiedProductBatch: SpecifiedProductBatch[];
}

class ApplicableCropProductionAgriculturalProcess {
  @ApiProperty()
  @ValidateNested()
  @Type(() => CodeType)
  @IsDefined()
  TypeCode: CodeType;
  @ApiProperty({ example: '2020-06-26T00:00:00Z' })
  @IsDateString()
  ActualStartDateTime: string;
  @ApiProperty({ example: '2020-06-26T00:00:00Z' })
  @IsDateString()
  ActualEndDateTime: string;
  @ApiProperty({ type: [AppliedSpecifiedAgriculturalApplication] })
  @ValidateNested({ each: true })
  @Type(() => AppliedSpecifiedAgriculturalApplication)
  @IsArray()
  @IsDefined()
  AppliedSpecifiedAgriculturalApplication: AppliedSpecifiedAgriculturalApplication[];
}



class GrownFieldCrop {
  @ApiProperty({ type: [ApplicableCropProductionAgriculturalProcess] })
  @ValidateNested({ each: true })
  @Type(() => ApplicableCropProductionAgriculturalProcess)
  @IsArray()
  @IsOptional()
  ApplicableCropProductionAgriculturalProcess?: ApplicableCropProductionAgriculturalProcess[];

  @ApiProperty()
  @Type(() => ClassificationCode)
  @ValidateNested()
  @IsOptional()
  ClassificationCode?: ClassificationCode;
}

class IncludedSpecifiedPolygon {
  @ApiProperty({example: 'Polygon'})
  @IsString()
  type: string;
  @ApiProperty({ type: [Number], example: [[[ 503697.99, 6505979.03]]] })
  @IsArray({ each: true })
  @IsDefined()
  coordinates: number[][][];
}

class PhysicalSpecifiedGeographicalFeature {
  @ApiProperty()
  @IsString()
  CoordinateReferenceSystemID: string;
  @ApiProperty()
  @ValidateNested()
  @Type(() => IncludedSpecifiedPolygon)
  @IsDefined()
  IncludedSpecifiedPolygon: IncludedSpecifiedPolygon;
}

class SpecifiedReferencedLocation {
  @ApiProperty()
  @ValidateNested()
  @Type(() => PhysicalSpecifiedGeographicalFeature)
  @IsDefined()
  PhysicalSpecifiedGeographicalFeature: PhysicalSpecifiedGeographicalFeature;
}

class CreateGrownCropPlot {
  @ApiProperty()
  @IsInt()
  ID: number;
  @ApiProperty()
  @ValidateNested()
  @Type(() => CodeType)
  @IsDefined()
  ObjectTypeCode: CodeType;
  @ApiProperty({ type: [GrownFieldCrop] })
  @ValidateNested({ each: true })
  @Type(() => GrownFieldCrop)
  @IsArray()
  @IsDefined()
  GrownFieldCrop: GrownFieldCrop[];

  @ApiProperty({ example: '34' })
  @IsNumberString()
  @IsOptional()
  ParcelNr?: string;

  @ApiProperty()
  @IsInt()
  @IsOptional()
  ReferenceParcelNr?: number;

  @ApiProperty({ type: [SpecifiedReferencedLocation] })
  @ValidateNested({ each: true })
  @Type(() => SpecifiedReferencedLocation)
  @IsOptional()
  @IsArray()
  SpecifiedReferencedLocation?: SpecifiedReferencedLocation[];
}

class ManagedAgriculturalProductionUnit {
  @ApiProperty()
  @ValidateNested()
  @Type(() => CreateGrownCropPlot)
  @IsDefined()
  GrownCropPlot: CreateGrownCropPlot;
}

class CreateAgriculturalProducerParty {
  @ApiProperty()
  @IsString()
  ID: string;
  @ApiProperty({ type: [ManagedAgriculturalProductionUnit] })
  @ValidateNested({ each: true })
  @Type(() => ManagedAgriculturalProductionUnit)
  @IsArray()
  @IsDefined()
  ManagedAgriculturalProductionUnit: ManagedAgriculturalProductionUnit[];
}

class CreateSpecifiedParty extends PickType(SpecifiedPartyInterface, ['ID'] as const) {}

class CreateCropReportDocument {
  @ApiProperty()
  @ValidateNested()
  @IsDefined()
  @Type(() => CreateSpecifiedParty)
  RecipientSpecifiedParty: CreateSpecifiedParty;
  @ApiProperty()
  @ValidateNested()
  @IsDefined()
  @Type(() => CreateSpecifiedParty)
  SenderSpecifiedParty: CreateSpecifiedParty;
}

export class CreateCropsDto {
  @ApiProperty()
  @ValidateNested()
  @IsDefined()
  @Type(() => CreateAgriculturalProducerParty)
  AgriculturalProducerParty: CreateAgriculturalProducerParty;
  @ApiProperty()
  @ValidateNested()
  @IsDefined()
  @Type(() => CreateCropReportDocument)
  CropReportDocument: CreateCropReportDocument;
}


