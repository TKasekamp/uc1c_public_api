import { Module } from '@nestjs/common';
import { ECropModule } from './eCrop/eCrop.module';
import configuration from './configuration';
import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [ECropModule,
    LoggerModule.forRoot({
      pinoHttp: {
        mixin() {
          return { module: 'NIVA' };
        },
      },
    }),
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    })],
})
export class AppModule {}
