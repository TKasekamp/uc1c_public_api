# NIVA UC1c Public API
## Description
Integration with Estonian registry.

Deployed at [Heroku](https://uc1c-public-api.herokuapp.com/)

Build with [Nest](https://github.com/nestjs/nest), a  TypeScript framework.


## Installation

```bash
$ npm install
```

## Running the app

Development uses [prettier logs](https://github.com/pinojs/pino-pretty).
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
Open browser and see swagger on [localhost](http://localhost:3000) .

## How to start the mock service as a container

Required software:
- docker (Docker Desktop is fine. https://www.docker.com/products/docker-desktop)
- git

Commands to start the application (Powershell must be used on Windows):

```bash
docker run --name niva --publish 127.0.0.1:80:3000 --rm -it $(docker build -q -f ./docker/Dockerfile .)
```

Browse to http://localhost:80


## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## License

  Nest is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).
